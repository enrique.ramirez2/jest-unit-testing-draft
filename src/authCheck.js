const axios = require("axios");

async function getTokenScopes(secrets) {
  let response = await axios({
    method: 'GET',
    url: `https://api.miro.com/v1/oauth-token`,
    headers: {
      'Authorization': `${secrets.auth_Authorization}`
    }
  });
  return response;
}

const checkTokenScopes = async (secrets, error) => {
  try {
    let response = await getTokenScopes(secrets);
    let tokenScopes = response.data.scopes;

    function hasEnterpriseScopes(tokenScopes) {
      const enterpriseScopes = ['auditlogs:read','boards:export','projects:read', 'projects:write', 'organizations:read',
        'organizations:teams:read', 'organizations:teams:write', 'sessions:delete'];

      return enterpriseScopes.some(scope => tokenScopes.includes(scope));
    }

    if (hasEnterpriseScopes(tokenScopes)) {
      error("Your API token is associated with an Enterprise Miro account. Please use the Enterprise Miro integration instead.");
    }

  } catch (err) {
    if (err.response && err.response.status === 400) {
      error("The request parameters are invalid. Please check your API token.");
    }
    else if (err.response && err.response.status === 401) {
      error("You couldn't be authenticated with those credentials. Please check your API token.");
    }
    else if (err.response && err.response.status === 500) {
      error("It was not possible to validate your token on Miro API. Please try again later.");
    }
    else {
      error(`There was an error in the checkTokenScopes helper. Error: ${err}`);
    }
  }
};

module.exports = async(input, callback, error) => {
  try {
    let request = input.request;
    let secrets = input.secrets;

    const test = () => console.log('Babel is working');
    test();

    await checkTokenScopes(secrets, error);

    callback(request);
  } catch (err) {
    error(`Pre-request script authCheck failed: ${err.message}`);
  }
};