const { defineConfig } = require("cypress");
// const junitReporter = require('cypress-junit-reporter');

module.exports = defineConfig({
  e2e: {
    setupNodeEvents(on, config) {
      require('@cypress/code-coverage/task')(on, config)

      // // Use the reporter
      // on('after:spec', (spec, results) => {
      //   junitReporter(spec, results, {
      //     junitReporterOptions: {
      //       mochaFile: 'results/my-test-output.xml'
      //     }
      //   });
      // });

      // It's IMPORTANT to return the config object
      // with any changed environment variables
      return config
    },
  },
})