const {sum, minus} = require('../src/sum.js');
const authentication = require('../src/authCheck.js');

describe('This module tests a sum function', () =>{
    it('Properly sums two numbers', () => {
        expect(sum(1,2)).toBe(3);
    });
    it('Properly sums a negative number', () => {
        expect(sum(1,-2)).toBe(-1);
    });
    it('Properly sums two negative numbers', () => {
        expect(sum(-1,-2)).toBe(-3);
    });
});

describe('This module tests a minus function', () =>{
    it('Properly subtracts two numbers', () => {
        expect(minus(1,2)).toBe(-1);
    });
});

describe('This module tests an authentication function', () => {
    let input = {
        payload: null,
        environment: null,
        headers: {},
        secrets: {
            auth_Authorization:"Bearer eyJtaXJvLm9yaWdpbiI6ImV1MDEifQ_cBdrDP1bh7S4UosPZHQ3n9irO_8", //Non-enterprise
        }
    };

    let callback = function (webhookRequest) {
        input.request = webhookRequest;
    };

    let error = function (name) {
        console.log("Failed: " + name);
    };

    it('Identifies a non-enterprise account', async () => {
        expect(await authentication(input, callback, error)).toBeUndefined();
    });
});

describe('This module tests an authentication function', () => {
    let input = {
        payload: null,
        environment: null,
        headers: {},
        secrets: {
            auth_Authorization:"Bearer eyJtaXJvLm9yaWdpbiI6ImV1MDEifQ_PGwGu7zQ3PA7V1asO1pSQ8EwYs4", //Enterprise
        }
    };

    let callback = function (webhookRequest) {
        input.request = webhookRequest;
    };

    let error = function (name) {
        console.log("Failed: " + name);
    };

    it('Identifies an enterprise account', async () => {
        expect(await authentication(input, callback, error)).isEmpty;// toBe(null);
    });
});

describe('This module tests an authentication function', () => {
    let input = {
        payload: null,
        environment: null,
        headers: {},
        secrets: {
            auth_Authorization:"Bearer eyJtaXJvLm9yaWdpbiI6ImV1MDE", //Broken token
        }
    };

    let callback = function (webhookRequest) {
        input.request = webhookRequest;
    };

    let error = function (name) {
        console.log("Failed: " + name);
    };

    it('Identifies a broken API token', async () => {
        expect(await authentication(input, callback, error)).isEmpty;// toBe(null);
    });
});

