import authentication from "../../instrument/authCheck.js/src/authCheck.js";

describe('Authentication check', () => {
  it('should identify a non-enterprise token correctly', () => {
    let input = {
      payload: null,
      environment: null,
      headers: {},
      secrets: {
        auth_Authorization:"Bearer eyJtaXJvLm9yaWdpbiI6ImV1MDEifQ_cBdrDP1bh7S4UosPZHQ3n9irO_8", //Non-enterprise
      }
    };

    let callback = function (webhookRequest) {
      input.request = webhookRequest;
    };

    let error = function (name) {
      console.log("Failed: " + name);
    };

    expect(authentication(input, callback, error)).to.not.be.null;
  });

  it('should identify an enterprise token correctly', () => {
    let input = {
      payload: null,
      environment: null,
      headers: {},
      secrets: {
        auth_Authorization:"Bearer eyJtaXJvLm9yaWdpbiI6ImV1MDE", //Enterprise
      }
    };

    let callback = function (webhookRequest) {
      input.request = webhookRequest;
    };

    let error = function (name) {
      console.log("Failed: " + name);
    };

    cy.wrap(authentication(input, callback, error)).should('not.be.null');
  });

  it('should error out if we provide a bad token', () => {
    let input = {
      payload: null,
      environment: null,
      headers: {},
      secrets: {
        auth_Authorization:"Bearer eyJtaXJvLm9yaWdpbiI6ImV1MDEifQ_PGwGu7zQ3PA7V1asO1pSQ8EwYs4", //Enterprise
      }
    };

    let callback = function (webhookRequest) {
      input.request = webhookRequest;
    };

    let error = function (name) {
      console.log("Failed: " + name);
    };

    cy.wrap(authentication(input, callback, error)).should('not.be.null');
  });

  it('should call the error callback with a message when the API returns a 401 error', () => {
    cy.intercept('GET', 'https://api.miro.com/v1/oauth-token', { statusCode: 401 }).as('getTokenScopes');
    cy.wrap(checkTokenScopes({ auth_Authorization: 'mocked_token' }, cy.stub())).then(() => {
      cy.get('@getTokenScopes').its('response.statusCode').should('eq', 401);
    });
  });
  it('should call the error callback with a message when the API returns a 400 error', () => {
    cy.intercept('GET', 'https://api.miro.com/v1/oauth-token', { statusCode: 400 }).as('getTokenScopes');
    cy.wrap(checkTokenScopes({ auth_Authorization: 'mocked_token' }, cy.stub())).then(() => {
      cy.get('@getTokenScopes').its('response.statusCode').should('eq', 400);
    });
  });
  it('should call the error callback with a message when the API returns a 500 error', () => {
    cy.intercept('GET', 'https://api.miro.com/v1/oauth-token', { statusCode: 500 }).as('getTokenScopes');
    cy.wrap(checkTokenScopes({ auth_Authorization: 'mocked_token' }, cy.stub())).then(() => {
      cy.get('@getTokenScopes').its('response.statusCode').should('eq', 500);
    });
  });
});

